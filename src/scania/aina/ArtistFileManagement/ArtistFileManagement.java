package scania.aina.ArtistFileManagement;

import scania.aina.GetArtistSet.GetArtistSet;

import javax.imageio.IIOException;
import java.io.*;
import java.util.HashSet;
import java.util.Iterator;

public class ArtistFileManagement {
    private String pathToFile = "myArtistList.txt";
    private HashSet<String> setReadFromFile = new HashSet<String>();
    private File file = new File(pathToFile);


    //create constructor input Artist set object
    public ArtistFileManagement(){

    }

    //Write method, create file included
    public void writeToArtistFile(HashSet<String> mySet) {

        //write content of set to txt file
        //intentionally set to overwrite rather than appen, hash set will ensure values are stored. Duplicates are prevented by doing like this.
            try(FileWriter input = new FileWriter(pathToFile, false);
                BufferedWriter bufferInput = new BufferedWriter(input)
            ){
                Iterator it = mySet.iterator();
                while(it.hasNext()){
                    bufferInput.write(it.next() + "\n");
                }

            }catch(IOException e){
                e.printStackTrace();
            }

    }
    //Read method
    public HashSet<String> readArtistFile(){


        try{
            boolean created = file.createNewFile();
        }catch(IOException e){
            e.printStackTrace();
        }

        try(FileReader input = new FileReader(pathToFile);
            BufferedReader reader = new BufferedReader(input)) {
            String line;
            while ((line = reader.readLine()) != null) {
                setReadFromFile.add(line);
            }
        }catch(IOException e){
                e.printStackTrace();
        }
        return setReadFromFile;

    }
    public String getFileSize(){
        return file.length() + " bytes";
    }
}
