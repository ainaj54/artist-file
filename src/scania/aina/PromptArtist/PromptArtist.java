package scania.aina.PromptArtist;

import scania.aina.GetArtistSet.GetArtistSet;

import java.util.Scanner;

public class PromptArtist {
    GetArtistSet newSet = new GetArtistSet();
    GetArtistSet readSetFromFile = new GetArtistSet();

    public PromptArtist() {

        readSetFromFile.readFromFile();
        readSetFromFile.printArtistSet();

        while (true) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("add favorite artist to your list");
            String inputLine = scanner.nextLine();

            if (inputLine.equals("exit")) {
                promptExit();
                break;
            } else if(inputLine.equals("display")){
                promptDisplay();

            } else if(inputLine.equals("clear")){
                promptClear();

            } else if(inputLine.equals("fileinfo")){
                promptFileInfo();

            }

            else {
                newSet.addArtist(inputLine);
            }
        }
    }
    public void promptExit(){
        readSetFromFile.addSetToSet(newSet);
        readSetFromFile.writeSetToFile();

    }
    public void promptDisplay(){
        newSet.printArtistSet();
    }
    public void promptClear(){
        newSet.clearArtistSet();
        newSet.printArtistSet();
        System.out.println("Input has been cleared");
    }
    public void promptFileInfo(){
        System.out.println(readSetFromFile.returnFileSize());
    }
}