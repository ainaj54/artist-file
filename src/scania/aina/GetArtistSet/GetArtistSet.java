package scania.aina.GetArtistSet;

import scania.aina.ArtistFileManagement.ArtistFileManagement;

import java.util.HashSet;
import java.util.List;
import java.util.Scanner;

public class GetArtistSet {
    private HashSet<String> artistSet = new HashSet<String>();
    private ArtistFileManagement newFileRead = new ArtistFileManagement();
    private ArtistFileManagement writeToFile = new ArtistFileManagement();
    private String readFileSize;
    
    public GetArtistSet(){
        
    }
    public GetArtistSet(String artist){
        artistSet.add(artist);
    }

    public void addArtist(String artist){
        artistSet.add(artist);
    }
    public void printArtistSet(){
        artistSet.forEach(System.out::println);
    }

    public void readFromFile(){

        this.artistSet = newFileRead.readArtistFile();
        this.readFileSize = newFileRead.getFileSize();

    }
    public String returnFileSize(){
        return this.readFileSize;
    }


    public void writeSetToFile(){
        writeToFile.writeToArtistFile(this.artistSet);
    }

    public void addSetToSet(GetArtistSet otherArtistSet){
        this.artistSet.addAll(otherArtistSet.artistSet);
    }
    public void clearArtistSet(){
        this.artistSet.clear();
    }


   
}
